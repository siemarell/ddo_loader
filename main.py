import pandas as pd
import pymssql


query_template = '''INSERT INTO [dbo].[T90730611]
           ([NAME]
           ,[PARENT_KEY]
           ,[ORD]
           ,[TER_LVL]
           ,[HIDDEN]
           ,[READACCESS]
           ,[WRITEACCESS]
           ,[DELETEACCESS]
           ,[ACCESSACCESS]
           ,[DIRECTOR]
           ,[OWN]
           ,[PHONE]
           ,[PROJECT]
           ,[YEAR]
           ,[TECH_CONDITION]
           ,[TYPE]
           ,[EMAIL]
           ,[WEB]
           ,[FUEL_TYPE]
           ,[MATERIAL])
     VALUES
           (%s
            ,%s/*PARENT_KEY*/
           ,%s		/*n=n+1, для каждого нового PARENT_KEY начинать заново*/
		   ,%s
           ,%s		/*FALSE*/
           ,%s
           ,%s
           ,%s
           ,%s
           ,%s
           ,%s		/*OWN*/
           ,%s
           ,%s
           ,%s
           ,%s
           ,%s
           ,%s
           ,%s
           ,%s
           ,%s
		   )'''

if __name__ == '__main__':
    conn = pymssql.connect(host='TS', user='sa', password='Qwerty1', database='reg_kyz1_data')
    cur = conn.cursor()

    pd.set_option('display.width', 1000)
    sheet = pd.read_excel('ddo.xls', sheetname='русс')
    sheet = sheet.fillna('')

    order = 1
    prev_parent_key = 0
    for i, data in sheet.iterrows():
        #if i == 0: print(type(data['OWN']))
        if data['PARENT_KEY'] == prev_parent_key:
            order += 1
        else:
            order = 1
            prev_parent_key = data['PARENT_KEY']
        data_tuple = (data['NAME'], data['PARENT_KEY'], order, 4, 0, 3,3,3,3,data['DIRECTOR'], data['OWN'],
              data['PHONE'], data['PROJECT'], data['YEAR'], data['TECH_CONDITION'], data['TYPE'],
              data['EMAIL'], data['WEB'], data['FUEL_TYPE'], data['MATERIAL'])
        cur.execute(query_template, data_tuple)

    conn.commit()